/// <reference types="cypress" />

// https://commons.wikimedia.org/wiki/File:Goblin.jpg Goblin image from here - thanks!

describe('basic functionality', () => {
  it('should work through a normal workflow without problems', () => {
    cy.visit('localhost:1234');

    cy.log(
      'Generate button should be disabled and there should be no minis when we first visit'
    );
    cy.get("[data-name='generate-button']").should('be.disabled');
    cy.get('[data-name=mini-card').should('not.exist');

    cy.log(
      'uploading an image should make a card for it and enable the generate button'
    );
    cy.get('[data-name=dropzone]').selectFile('cypress/fixtures/Goblin.jpg', {
      action: 'drag-drop',
    });
    cy.get("[data-name='generate-button']").should('not.be.disabled');
    cy.get('[data-name=mini-card').should('have.length', 1);

    cy.log('uploading another image should work');
    cy.get('[data-name=dropzone]').selectFile('cypress/fixtures/Goblin.jpg', {
      action: 'drag-drop',
    });
    cy.get("[data-name='generate-button']").should('not.be.disabled');
    cy.get('[data-name=mini-card').should('have.length', 2);

    cy.log('should be able to edit settings');
    cy.get('[data-name=settings-dialog').should('not.exist');
    cy.get('[data-name=settings-button]').click();
    cy.get('[data-name=settings-dialog').should('exist');
    cy.get('[data-name=settings-title-input').type('Test PDF');
    cy.get('[data-name=save-settings-button').click();
    cy.get('[data-name=settings-dialog').should('not.exist');

    cy.log('we should be able to download the PDF');
    cy.get('[data-name=generate-button]').click();
    cy.get('[data-name=download-link]').should('have.attr', 'href');
  });
});
