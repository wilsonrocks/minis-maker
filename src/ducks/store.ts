import { configureStore } from '@reduxjs/toolkit';
import minisReducer from './minisDuck';
import settingsReducer from './settingsDuck';

const store = configureStore({
  reducer: { settings: settingsReducer, minis: minisReducer },
});

export type RootState = ReturnType<typeof store.getState>;

export default store;
