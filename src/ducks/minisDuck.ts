import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { nanoid } from 'nanoid';

export interface Mini {
  autocrop: boolean;
  baseColor: string;
  baseHeight: number;
  width: number;
  id: string;
  originalImageUrl: string | null;
  quantity: number;
  croppedImageUrl: string | null;
  flipAlternatingMinis: boolean;
  title: string;
  showFoldLine: boolean;
  showTitle: boolean;
}

export type MiniWithIdentifier = Mini & { identifier: number };

const initialState: Mini[] = [];

export const newBlankMini: (title: string, imageUrl: string) => Mini = (
  title,
  imageUrl
) => ({
  autocrop: false,
  baseColor: '#000000',
  baseHeight: 13,
  flipAlternatingMinis: false,
  id: nanoid(),
  originalImageUrl: imageUrl,
  quantity: 1,
  croppedImageUrl: imageUrl,
  showFoldLine: false,
  showTitle: false,
  title,
  width: 25,
});

const minisSlice = createSlice({
  name: 'minis',
  initialState,
  reducers: {
    newMini(
      state,
      action: PayloadAction<{ title: string; imageUrl: string; file: File }>
    ) {
      const { title, imageUrl } = action.payload;

      const newMini = newBlankMini(title, imageUrl);

      state.push(newMini);
    },
    deleteMini(state, action: PayloadAction<string>) {
      const relevantMiniIndex = state.findIndex(
        (mini) => mini.id === action.payload
      );
      window.imageBuffers.delete(state[relevantMiniIndex].id);
      if (relevantMiniIndex !== -1) state.splice(relevantMiniIndex, 1);
    },
    changeMini(
      state,
      action: PayloadAction<{
        id: string;
        changedData: Partial<Omit<Mini, 'id'>>;
      }>
    ) {
      let relevantMiniIndex = state.findIndex(
        (mini) => mini.id === action.payload.id
      );

      if (relevantMiniIndex === -1) {
        return;
      } else {
        state[relevantMiniIndex] = {
          ...state[relevantMiniIndex],
          ...action.payload.changedData,
        };
      }
    },
  },
});

const { actions, reducer: minisReducer } = minisSlice;

export const { newMini, deleteMini, changeMini } = actions;

export default minisReducer;
