import store from './store';

import { FunctionComponent } from 'react';
import { Provider } from 'react-redux';

const StoreProvider: FunctionComponent = ({ children }) => (
  <Provider store={store}>{children}</Provider>
);

export default StoreProvider;
