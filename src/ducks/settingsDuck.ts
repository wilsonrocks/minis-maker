import { Paper } from '@mantine/core';
import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { EnumType } from 'typescript';

export enum PaperSize {
  A4 = 'A4',
  Letter = 'Letter',
}

export const isPaperSize = (str: string): str is PaperSize => {
  return Object.values(PaperSize).includes(str as PaperSize);
};

export type Length = 'mm' | 'in';

export interface Settings {
  paperSize: PaperSize;
  title: string;
  author: string;
  spacing: number;
  margin: number;
  foldLine: boolean;
}

const initialState: Settings = {
  paperSize: PaperSize.A4,
  author: '',
  title: '',
  spacing: 2,
  margin: 5,
  foldLine: false,
};

const settingsSlice = createSlice({
  name: 'settings',
  initialState,
  reducers: {
    changeSettings(state, action: PayloadAction<Partial<Settings>>) {
      return { ...state, ...action.payload };
    },
  },
});

const { actions, reducer: settingsReducer } = settingsSlice;

export const { changeSettings } = actions;

export default settingsReducer;
