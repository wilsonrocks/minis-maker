import * as Comlink from 'comlink';
import { GeneratePdfFromMinis } from './pdf.worker';

export const makePdf: Comlink.Remote<GeneratePdfFromMinis> = Comlink.wrap(
  new Worker(new URL('pdf.worker.ts', import.meta.url), { type: 'module' })
);
