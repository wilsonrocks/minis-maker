import { settings } from 'cluster';
import Jimp from 'jimp';
import { Mini } from '../ducks/minisDuck';
import { PaperSize, Settings } from '../ducks/settingsDuck';
import { packMinis, Solution } from './packing';

const MM_TO_PT = 72 / 25.4;
const PT_TO_PX = 300 / 72;

const PAPER_SIZE: Record<
  PaperSize,
  {
    width: number;
    height: number;
  }
> = {
  A4: {
    width: 595,
    height: 842,
  },
  Letter: { width: 612, height: 792 },
};

interface MiniToPlace {
  flippedImage: string;
  scaledImage: string;
  heightPts: number;
  widthPts: number;
  baseHeightPts: number;
  baseColor: string;
}

const processMinis = async (minis: Mini[]): Promise<MiniToPlace[]> => {
  let processedMinis: MiniToPlace[] = [];

  for (const mini of minis) {
    const { croppedImageUrl } = mini;
    if (croppedImageUrl === null)
      throw new Error('this should not be null, yet it is ');
    const jimpImage = await Jimp.read(croppedImageUrl);

    const widthPts = mini.width * MM_TO_PT;

    const scaledImage = jimpImage.resize(widthPts * PT_TO_PX, Jimp.AUTO); // TODO what DPI etc ?
    const heightToWidthRatio = scaledImage.getHeight() / scaledImage.getWidth();
    const heightPts = heightToWidthRatio * widthPts;

    const scaledImageUrl = await scaledImage.getBase64Async(
      scaledImage.getMIME()
    );

    const flippedScaledImage = scaledImage.flip(false, true);

    const flippedScaledImageUrl = await flippedScaledImage.getBase64Async(
      flippedScaledImage.getMIME()
    );

    const minisToAdd: MiniToPlace[] = new Array<MiniToPlace>(
      mini.quantity
    ).fill({
      heightPts,
      widthPts,
      scaledImage: scaledImageUrl,
      flippedImage: flippedScaledImageUrl,
      baseHeightPts: mini.baseHeight * MM_TO_PT,
      baseColor: mini.baseColor,
    });

    processedMinis = [...processedMinis, ...minisToAdd];
  }
  return processedMinis;
};

export const addMinisToDocument = async (
  document: PDFKit.PDFDocument,
  minis: Mini[],
  settings: Settings
): Promise<void> => {
  const trueMargin = (settings.margin - settings.spacing) * MM_TO_PT;

  const minisToPlace = await processMinis(minis);
  const solution = packMinis<MiniToPlace>({
    minisToPlace,
    paperWidth: PAPER_SIZE[settings.paperSize].width - 2 * trueMargin,
    paperHeight: PAPER_SIZE[settings.paperSize].height - 2 * trueMargin,
    spacing: settings.spacing * MM_TO_PT,
    getDimensions: ({ heightPts, widthPts, baseHeightPts }) => ({
      height:
        heightPts * 2 +
        2 * baseHeightPts +
        (settings.foldLine ? 1 * MM_TO_PT : 0),
      width: widthPts,
    }),
  });

  addSolutionToDocument(solution, document, settings);
};

const addSolutionToDocument = (
  solution: Solution<MiniToPlace>,
  document: PDFKit.PDFDocument,
  settings: Settings
): void => {
  const offset = settings.margin - settings.spacing;
  for (const page of solution) {
    document.addPage();
    for (const { item, x, y } of page) {
      let cumulativeY = 0;
      document
        .rect(
          offset + x,
          offset + y + cumulativeY,
          item.widthPts,
          item.baseHeightPts
        )
        .fill(item.baseColor);
      cumulativeY += item.baseHeightPts;

      document.image(item.flippedImage, offset + x, offset + y + cumulativeY, {
        width: item.widthPts,
      });
      cumulativeY += item.heightPts;

      if (settings.foldLine) {
        document
          .rect(
            offset + x,
            offset + y + cumulativeY,
            item.widthPts,
            1 * MM_TO_PT
          )
          .fill('#CCCCCC');

        cumulativeY += 1 * MM_TO_PT;
      }

      document.image(item.scaledImage, offset + x, offset + y + cumulativeY, {
        width: item.widthPts,
      });
      cumulativeY += item.heightPts;

      document
        .rect(
          offset + x,
          offset + y + cumulativeY,
          item.widthPts,
          item.baseHeightPts
        )
        .fill(item.baseColor);
      cumulativeY += item.baseHeightPts;

      document
        .rect(offset + x, offset + y, item.widthPts, cumulativeY)
        .stroke('black');
    }
  }
};
