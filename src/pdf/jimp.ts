import * as Comlink from 'comlink';
import { JimpWorker } from './jimp.worker';

export const imageHandler: Comlink.Remote<JimpWorker> = Comlink.wrap(
  new Worker(new URL('jimp.worker.ts', import.meta.url), {
    type: 'module',
  })
);
