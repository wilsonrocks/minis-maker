import * as Comlink from 'comlink';
import jimp from 'jimp';
import { Mini } from '../ducks/minisDuck';

async function rotateMini(mini: Mini): Promise<string> {
  if (mini.originalImageUrl === null) throw new Error('null image!');

  const dataUrl = await jimp
    .read(mini.originalImageUrl)
    .then((image) => image.rotate(180))
    .then((image) => image.getBase64Async(image.getMIME()));

  return dataUrl;
}

async function resizeMini(mini: Mini): Promise<string> {
  if (mini.originalImageUrl === null) throw new Error('null image!');
  const newWidth = (mini.width * 300) / 25.4;

  const blobUrl = await jimp
    .read(mini.originalImageUrl)
    .then((image) => image.resize(newWidth, jimp.AUTO))
    .then((image) => image.getBase64Async(image.getMIME()))
    .then((base64) => fetch(base64))
    .then((response) => response.blob())
    .then((blob) => URL.createObjectURL(blob));
  return blobUrl;
}

async function rotateImage(url: string): Promise<string> {
  return jimp
    .read(url)
    .then((image) => image.rotate(180))
    .then((image) => image.getBase64Async(image.getMIME()))
    .then((base64) => fetch(base64))
    .then((response) => response.blob())
    .then((blob) => URL.createObjectURL(blob));
}

async function getHeightToWidthRatio(url: string): Promise<number> {
  return jimp.read(url).then((image) => image.getHeight() / image.getWidth());
}

const output = { rotateMini, resizeMini, rotateImage, getHeightToWidthRatio };

export type JimpWorker = typeof output;

Comlink.expose(output);
