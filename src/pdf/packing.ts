import produce, { createDraft, current } from 'immer';

interface Dimensions {
  height: number;
  width: number;
}

interface Position {
  x: number;
  y: number;
}

type Item<T> = Position & { item: T };
type Gap = Position & {
  filled: boolean;
} & Dimensions;
type SolutionWithGaps<T> = (Item<T> | Gap)[][];

export type Solution<T> = Item<T>[][];

const isGap = <T>(x: Item<T> | Gap): x is Gap =>
  (x as Gap).filled !== undefined;
const isItem = <T>(x: Item<T> | Gap): x is Item<T> =>
  (x as Gap).filled === undefined;

const blankPage = (pageWidth: number, pageHeight: number): Gap => ({
  x: 0,
  y: 0,
  filled: false,
  width: pageWidth,
  height: pageHeight,
});

interface PackMinis<T> {
  minisToPlace: T[];
  paperWidth: number;
  paperHeight: number;
  spacing: number;
  getDimensions: (t: T) => Dimensions;
}

export const packMinis = <T>({
  minisToPlace,
  paperWidth,
  paperHeight,
  spacing,
  getDimensions,
}: PackMinis<T>): Solution<T> => {
  const sortedMinisToPlace = minisToPlace.sort((miniA, miniB): number => {
    const { height: heightA, width: widthA } = getDimensions(miniA);
    const { height: heightB, width: widthB } = getDimensions(miniB);
    return Math.max(heightB, widthB) - Math.max(heightA, widthA);
  });

  const solution: SolutionWithGaps<T> = sortedMinisToPlace.reduce(
    (solutionSoFar: SolutionWithGaps<T>, mini: T) => {
      const { height: miniHeight, width: miniWidth } = getDimensions(mini);

      return produce(solutionSoFar, (draft) => {
        for (let page of draft) {
          for (let itemOrGap of page) {
            if (!isGap(itemOrGap)) continue;

            const gap = itemOrGap;
            if (gap.filled) continue;

            const widthFits = miniWidth + spacing <= gap.width;
            const heightFits = miniHeight + spacing <= gap.height;

            const miniFitsInGap = heightFits && widthFits;

            if (miniFitsInGap) {
              gap.filled = true;
              // TODO extract this logic out so it can be used when we spill over to a new page too
              page.push({
                x: gap.x + spacing,
                y: gap.y + spacing,
                item: createDraft(mini),
              });

              const gapBelow: Gap = {
                filled: false,
                x: gap.x,
                y: gap.y + spacing + miniHeight,
                width: gap.width,
                height: gap.height - (spacing + miniHeight),
              };

              page.push(gapBelow);

              const gapRight: Gap = {
                filled: false,
                x: gap.x + spacing + miniWidth,
                y: gap.y,
                width: gap.width - (spacing + miniWidth),
                height: spacing + miniHeight,
              };

              page.push(gapRight);

              return draft;
            }
          }
        }
        // so now we've been through every gap on every page

        // we need a new page

        const newPage: (Item<T> | Gap)[] = [
          { x: spacing, y: spacing, item: mini },
          {
            // to the right
            filled: false,
            x: spacing + miniWidth,
            y: 0,

            width: paperWidth - (spacing + miniWidth),
            height: spacing + miniHeight,
          },
          {
            // to the bottom
            filled: false,
            x: 0,
            y: spacing + miniHeight,

            width: paperWidth,
            height: paperHeight - (spacing + miniHeight),
          },
        ];
        draft.push(createDraft(newPage));
        return draft;
      });
    },
    [[blankPage(paperWidth, paperHeight)]]
  );

  const gaplessSolution: Item<T>[][] = solution.map((page) =>
    page.filter(isItem)
  );
  return gaplessSolution;
};
