import PdfDocument from 'pdfkit';
import blobStream from 'blob-stream';
import * as Comlink from 'comlink';

import { Mini } from '../ducks/minisDuck';
import { Settings } from '../ducks/settingsDuck';
import { addMinisToDocument } from './addMinisToDocument';
import fileSize from 'file-size';

const generatePdfFromMinis = async (
  minis: Mini[],
  settings: Settings
): Promise<{ url: string; size: string }> => {
  const document = new PdfDocument({
    size: settings.paperSize,
    autoFirstPage: false,
    info: {
      Author: settings.author,
      Title: settings.title,
      Producer: "James's Gaming Website",
    },
  });

  const stream = document.pipe(blobStream());
  await addMinisToDocument(document, minis, settings);

  document.end();
  return new Promise((resolve, reject) => {
    stream.on('finish', () => {
      const pdfFile = stream.toBlob();

      const size = fileSize(pdfFile.size, { fixed: 1 }).human('si');
      const url = URL.createObjectURL(pdfFile);

      resolve({ url, size });
    });
    stream.on('error', (err: unknown) => {
      reject(err);
    });
  });
};

export type GeneratePdfFromMinis = typeof generatePdfFromMinis;

Comlink.expose(generatePdfFromMinis);
