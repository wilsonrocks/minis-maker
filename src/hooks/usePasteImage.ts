import React, { useEffect, useRef } from 'react';

export const usePasteImage = (
  onPasteImage: (file: File) => void
): React.RefObject<HTMLDivElement> => {
  const ref = useRef<HTMLDivElement>(null);

  useEffect(() => {
    const element = ref.current;
    if (element === null) return;

    const handler = (event: ClipboardEvent) => {
      const { clipboardData } = event;
      if (clipboardData === null) return;
      const files = Object.values(clipboardData.items)
        .filter((x) => x.kind === 'file')
        .map((x) => x.getAsFile());

      files.forEach((file) => {
        if (file === null) return;
        onPasteImage(file);
      });
    };

    element.addEventListener('paste', handler);

    return () => {
      element.removeEventListener('paste', handler);
    };
  }, []);
  return ref;
};
