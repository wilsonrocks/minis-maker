import { Container } from '@mantine/core';
import React from 'react';
import StoreProvider from './ducks/provider';
import Forms from './forms/forms';
import 'react-image-crop/dist/ReactCrop.css';

function App() {
  return (
    <StoreProvider>
      <Container>
        <Forms />
      </Container>
    </StoreProvider>
  );
}

export default App;
