import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import { Mini } from './ducks/minisDuck';

ReactDOM.render(<App />, document.getElementById('root'));

window.imageBuffers = new Map<string, ArrayBuffer>();
