import { D20_WIDTH_MM } from '../config';
import { Mini } from '../ducks/minisDuck';
import d20Png from './d20.png';

interface MiniImageWithDiceProps {
  mini: Mini;
  showDice: boolean;
}

export const MiniImageWithDice = ({
  mini,
  showDice,
}: MiniImageWithDiceProps) => (
  <div style={{ position: 'relative' }}>
    <img
      style={{ width: '100%', objectFit: 'scale-down' }}
      src={mini.croppedImageUrl ?? undefined}
    />
    <img
      src={d20Png}
      style={{
        position: 'absolute',
        bottom: 0,
        right: 0,
        width: `${(100 * D20_WIDTH_MM) / mini.width}%`,
        opacity: showDice ? 1 : 0,
        transition: 'opacity 0.2s, width 0.2s',
      }}
      title="Licensed from CherubAgent1440 under Creative Commons CC BY-SA 4.0. (https://creativecommons.org/licenses/by-sa/4.0/)"
    />
  </div>
);
