import Jimp from 'jimp';
import { PercentCrop } from 'react-image-crop';

export function resizeImage(
  originalUrl: string,
  miniWidth: number
): Promise<string> {
  return Jimp.read(originalUrl)
    .then((image) => image.resize(miniWidth, Jimp.AUTO))
    .then((image) => image.getBase64Async('image/png'))
    .then((base64) => fetch(base64))
    .then((response) => response.blob())
    .then((blob) => URL.createObjectURL(blob));
}

export function cropImage(
  originalUrl: string,
  {
    x: xPercent,
    y: yPercent,
    width: widthPercent,
    height: heightPercent,
  }: PercentCrop
): Promise<string> {
  return Jimp.read(originalUrl)
    .then((image) => {
      const x = (xPercent / 100) * image.getWidth();
      const y = (yPercent / 100) * image.getHeight();
      const width = (widthPercent / 100) * image.getWidth();
      const height = (heightPercent / 100) * image.getHeight();

      return image.crop(x, y, width, height);
    })
    .then((image) => image.getBase64Async('image/png'))
    .then((base64) => fetch(base64))
    .then((response) => response.blob())
    .then((blob) => URL.createObjectURL(blob));
}
