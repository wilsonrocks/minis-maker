import { Center, Container, Grid, Text, Title } from '@mantine/core';
import { Dropzone, MIME_TYPES } from '@mantine/dropzone';
import { ImImages } from 'react-icons/im';
import { useDispatch, useSelector } from 'react-redux';
import { newMini } from '../ducks/minisDuck';
import { RootState } from '../ducks/store';
import { usePasteImage } from '../hooks/usePasteImage';
import { MiniCard } from './miniCard';
import { Navigation } from './Navigation';

const Forms = () => {
  const { minis } = useSelector<RootState, RootState>((state) => state);

  const dispatch = useDispatch();

  const addNewMini = (file: File) => {
    const imageUrl = URL.createObjectURL(file);
    const title = file.name;
    dispatch(newMini({ title, imageUrl, file }));
  };

  const pdfPasteRef = usePasteImage(addNewMini);
  return (
    <Container ref={pdfPasteRef}>
      <Grid align="stretch">
        <Grid.Col md={4}>
          <Title>James's Gaming Website</Title>
        </Grid.Col>
        <Grid.Col md={8}>
          <Navigation />
        </Grid.Col>

        {minis.map((mini) => (
          <Grid.Col key={mini.id} md={6} lg={4}>
            <MiniCard mini={mini} key={mini.id} />
          </Grid.Col>
        ))}

        <Grid.Col md={6} lg={4}>
          <Dropzone
            data-name="dropzone"
            sx={{ height: '100%' }}
            onDrop={(files) => {
              files.forEach(addNewMini);
            }}
            onReject={(files) => console.error(files.toString())}
            accept={[
              MIME_TYPES.png,
              MIME_TYPES.jpeg,
              MIME_TYPES.svg,
              MIME_TYPES.gif,
            ]}
          >
            {() => (
              <Grid>
                <Grid.Col xs={2}>
                  <Center>
                    <ImImages size="50px" fill="gray" />
                  </Center>
                </Grid.Col>
                <Grid.Col xs={10}>
                  <Text size="xl" sx={{ flexGrow: 10 }}>
                    Drag images here or click to select files, or paste from
                    your clipboard
                  </Text>
                </Grid.Col>
              </Grid>
            )}
          </Dropzone>
        </Grid.Col>
      </Grid>
    </Container>
  );
};

export default Forms;
