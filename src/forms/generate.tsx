import { Button, Group, Loader, Modal, Text, Title } from '@mantine/core';
import { useEffect } from 'react';
import { useState } from 'react';
import { useSelector } from 'react-redux';
import { RootState } from '../ducks/store';
import { makePdf } from '../pdf/output';

class GeneratePdfError extends Error {
  constructor(message: string, duration: number) {
    super();
    this.message = message;
    this.duration = duration;
  }
  duration: number;
}

type CurrentState = 'button' | 'generating' | 'generated' | 'error';

export const Generate = () => {
  const [currentState, setCurrentState] = useState<CurrentState>('button');
  const [results, setResults] = useState<{ url: string; size: string } | null>(
    null
  );
  const [duration, setDuration] = useState<number | null>(null);

  const { minis, settings } = useSelector<RootState, RootState>(
    (state) => state
  );
  useEffect(() => {
    if (currentState === 'generating') {
      const startTimestamp = performance.now();
      makePdf(minis, settings)
        .then(({ url, size }) => {
          setResults({ url, size });
          setCurrentState('generated');
          setDuration(performance.now() - startTimestamp);
        })
        .catch((err) => {
          const duration = performance.now() - startTimestamp;
          const customError = new GeneratePdfError(
            'problem generating PDF',
            duration
          );
          customError.cause = err;
          console.error(customError);
          setCurrentState('error');
        });
    }
  }, [currentState]);
  return (
    <>
      <Button
        disabled={minis.length === 0}
        onClick={() => setCurrentState('generating')}
        data-name="generate-button"
      >
        Generate PDF
      </Button>
      <Modal
        opened={currentState !== 'button'}
        onClose={() => setCurrentState('button')}
      >
        {currentState === 'generating' && (
          <Group direction="column" position="center">
            <Title>Your minis await</Title>
            <Text>Generating...</Text>
            <Loader />
          </Group>
        )}
        {currentState === 'generated' && (
          <Group direction="column" position="center">
            <Title>Your minis await</Title>
            <Text>
              Done{duration ? ` in ${(duration / 1000).toFixed(1)}s` : ''}!
            </Text>
            <Button
              component="a"
              data-name="download-link"
              href={results?.url ?? '#'}
              download={`${settings.title || 'Paper Minis'}.pdf`}
            >
              Get the PDF{results ? ` (${results.size})` : ''}
            </Button>
            <Text>
              (if you want to share this PDF, you need to download it now as the
              link won't work for anybody else, or you if you close your tab)
            </Text>
          </Group>
        )}
        {currentState === 'error' && (
          <Group direction="column" position="center">
            <Title>Something went wrong!</Title>
            <Text>
              The browser console (press F12, click console) should have more
              information which James would like to know to help him fix it.
            </Text>
            <Button
              onClick={() => {
                setCurrentState('button');
              }}
            >
              Close
            </Button>
          </Group>
        )}
      </Modal>
    </>
  );
};
