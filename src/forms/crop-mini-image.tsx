import { Modal } from '@mantine/core';
import { useState } from 'react';
import ReactCrop, { PercentCrop } from 'react-image-crop';
import { useDispatch } from 'react-redux';
import { changeMini, Mini } from '../ducks/minisDuck';
import { cropImage } from './image.worker';

interface CropMiniImageProps {
  isCropping: boolean;
  setIsCropping: (isCropping: boolean) => void;
  mini: Mini;
}

export const CropMiniImage = ({
  isCropping,
  setIsCropping,
  mini,
}: CropMiniImageProps) => {
  const dispatch = useDispatch();
  const [currentCrop, setCurrentCrop] = useState<PercentCrop>({
    x: 0,
    y: 0,
    width: 100,
    height: 100,
    unit: '%',
  });
  const { originalImageUrl } = mini;
  if (originalImageUrl === null) return null;
  return (
    <Modal
      opened={isCropping}
      onClose={async () => {
        setIsCropping(false);
        const croppedImageUrl = await cropImage(originalImageUrl, currentCrop);
        dispatch(
          changeMini({
            id: mini.id,
            changedData: { croppedImageUrl: croppedImageUrl },
          })
        );
      }}
      data-name="crop-mini-image"
    >
      <ReactCrop
        onChange={(pixelCrop, percentCrop) => setCurrentCrop(percentCrop)}
        crop={currentCrop}
      >
        <img src={mini.originalImageUrl ?? undefined} />
      </ReactCrop>
    </Modal>
  );
};
