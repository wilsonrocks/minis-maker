import {
  Button,
  Checkbox,
  Group,
  Modal,
  NumberInput,
  Select,
  TextInput,
  Title,
} from '@mantine/core';
import { useDispatch, useSelector } from 'react-redux';
import { changeSettings, isPaperSize, PaperSize } from '../ducks/settingsDuck';
import { RootState } from '../ducks/store';

export const Settings: React.FC<{
  isShowingSettings: boolean;
  setIsShowingSettings: (x: boolean) => void;
}> = ({ isShowingSettings, setIsShowingSettings }) => {
  const { minis, settings } = useSelector<RootState, RootState>(
    (state) => state
  );
  const dispatch = useDispatch();

  return (
    <Modal
      opened={isShowingSettings}
      onClose={() => {
        setIsShowingSettings(false);
      }}
      data-name="settings-dialog"
    >
      <Title>Settings</Title>
      <Group direction="column">
        <Select
          label="Paper size"
          data={Object.values(PaperSize)}
          value={settings.paperSize}
          onChange={(paperSize) => {
            if (paperSize && isPaperSize(paperSize)) {
              dispatch(changeSettings({ paperSize }));
            }
          }}
        />
        <NumberInput
          label="Spacing between minis (mm)"
          value={settings.spacing}
          onChange={(value) => {
            if (value) dispatch(changeSettings({ spacing: value }));
          }}
          min={0}
        />
        <NumberInput
          label="Margin (mm)"
          value={settings.margin}
          onChange={(value) => {
            if (value) dispatch(changeSettings({ margin: value }));
          }}
          min={0}
        />
        <TextInput
          data-name="settings-title-input"
          label="Title"
          value={settings.title}
          onChange={(event) =>
            dispatch(changeSettings({ title: event.target.value }))
          }
        />
        <TextInput
          label="Author"
          value={settings.author}
          onChange={(event) =>
            dispatch(changeSettings({ author: event.target.value }))
          }
        />
        <Checkbox
          label="Show fold line"
          checked={settings.foldLine}
          onChange={(event) => {
            dispatch(changeSettings({ foldLine: event.currentTarget.checked }));
          }}
        />
        <Button
          data-name="save-settings-button"
          onClick={() => setIsShowingSettings(false)}
        >
          Save
        </Button>
      </Group>
    </Modal>
  );
};
