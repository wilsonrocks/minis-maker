import { Button, Grid, Group, Navbar, Text } from '@mantine/core';
import { useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { RootState } from '../ducks/store';
import { Generate } from './generate';
import { Settings } from './Settings';

export const Navigation = () => {
  const { minis } = useSelector<RootState, RootState>((state) => state);
  const totalMinis = minis.reduce(
    (totalSoFar, mini) => totalSoFar + mini.quantity,
    0
  );
  const [isShowingSettings, setIsShowingSettings] = useState<boolean>(false);

  return (
    <Grid>
      <Grid.Col sm={6}>
        <Group>
          <Button
            variant="outline"
            onClick={() => {
              setIsShowingSettings(true);
            }}
            data-name="settings-button"
          >
            Settings
          </Button>
          <Settings
            isShowingSettings={isShowingSettings}
            setIsShowingSettings={setIsShowingSettings}
          />
          {/* Make this one component like Generate*/}
          <Generate />
        </Group>
      </Grid.Col>

      <Grid.Col sm={12}>
        <Text>
          {totalMinis} minis of {minis.length} designs.
        </Text>
      </Grid.Col>
    </Grid>
  );
};
