import {
  Button,
  Card,
  Center,
  ColorInput,
  Grid,
  Group,
  NumberInput,
  Switch,
} from '@mantine/core';
import { useState } from 'react';
import { useDispatch } from 'react-redux';
import { MiniImageWithDice } from '../components/miniImageWithDice';
import { changeMini, deleteMini, Mini } from '../ducks/minisDuck';
import { CropMiniImage } from './crop-mini-image';

interface MiniCardProps {
  mini: Mini;
}

export const MiniCard = ({ mini }: MiniCardProps) => {
  const dispatch = useDispatch();
  const [isCropping, setIsCropping] = useState<boolean>(false);
  const [showDice, setShowDice] = useState<boolean>(true);

  return (
    <>
      <CropMiniImage
        isCropping={isCropping}
        setIsCropping={setIsCropping}
        mini={mini}
      />
      <form>
        <Card shadow="sm" withBorder data-name="mini-card">
          <Grid>
            <Grid.Col xs={6}>
              <MiniImageWithDice mini={mini} showDice={showDice} />
              <Center>
                <Group>
                  <Button
                    onClick={() => {
                      setIsCropping(true);
                    }}
                  >
                    Crop
                  </Button>
                  <Button
                    color="red"
                    onClick={() => {
                      dispatch(deleteMini(mini.id));
                    }}
                  >
                    Delete mini
                  </Button>
                  <Switch
                    checked={showDice}
                    onChange={() => setShowDice(!showDice)}
                    label="Show d20"
                  />
                </Group>
              </Center>
            </Grid.Col>

            <Grid.Col xs={6}>
              <Group direction="column">
                <NumberInput
                  label="Quantity"
                  value={mini.quantity}
                  min={1}
                  step={1}
                  onChange={(value) =>
                    dispatch(
                      changeMini({
                        id: mini.id,
                        changedData: { quantity: value },
                      })
                    )
                  }
                />
                <NumberInput
                  label="Width of mini (mm)"
                  value={mini.width}
                  onChange={(value) =>
                    dispatch(
                      changeMini({
                        id: mini.id,
                        changedData: { width: value },
                      })
                    )
                  }
                />
                <NumberInput
                  label="Height of base (mm)"
                  value={mini.baseHeight}
                  onChange={(value) =>
                    dispatch(
                      changeMini({
                        id: mini.id,
                        changedData: { baseHeight: value },
                      })
                    )
                  }
                  min={0.01}
                  step={0.01}
                />
                <ColorInput
                  label="Color of base"
                  value={mini.baseColor}
                  onChange={(value) =>
                    dispatch(
                      changeMini({
                        id: mini.id,
                        changedData: { baseColor: value },
                      })
                    )
                  }
                />
                {/*
              <Checkbox
                label="Flip alternating minis"
                checked={mini.flipAlternatingMinis}
                onChange={(event) =>
                  dispatch(
                    changeMini({
                      id: mini.id,
                      changedData: { flipAlternatingMinis: event.target.checked },
                    })
                  )
                }
              />
              <Checkbox
                label="Show fold line"
                checked={mini.showFoldLine}
                onChange={(event) =>
                  dispatch(
                    changeMini({
                      id: mini.id,
                      changedData: { showFoldLine: event.target.checked },
                    })
                  )
                }
              />
              <Checkbox
                label="Show title"
                checked={mini.showTitle}
                onChange={(event) =>
                  dispatch(
                    changeMini({
                      id: mini.id,
                      changedData: { showTitle: event.target.checked },
                    })
                  )
                }
              /> */}
              </Group>
            </Grid.Col>
          </Grid>
        </Card>
      </form>
    </>
  );
};
